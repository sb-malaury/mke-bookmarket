/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.tncy.mke.bookmarket.data;

/**
 *
 * @author student
 */
public class Book {
    
    enum BookFormat{BROCHE,POCHE};
    int id;
    String title;
    String author;
    String publisher;
    BookFormat format;
    String isbn;
    
    public String getTitle(){
        return this.title;
    }
    
    public String getAuthor(){
        return this.author;
    }
    
    public String getPublisher(){
        return this.publisher;
    }
}
